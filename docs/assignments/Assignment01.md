# Basic sequence analysis

## A short introduction
DNA sequencing is a process to determine the exact sequence of nucleotides in a DNA molecule. The process involves several steps, from the wet-lab to chromatograms, and requires specialized equipment and software to extract meaningful data.

In the wet-lab, DNA sequencing begins by isolating and amplifying the DNA of interest. This is typically done through a process called polymerase chain reaction (PCR), which copies the DNA millions of times. Once amplified, the DNA is ready for sequencing.

The sequencing process itself is typically performed using capillary electrophoresis, a technique that separates DNA fragments based on size and charge. In capillary electrophoresis, a DNA sample is loaded into a capillary tube filled with a gel matrix. An electric current is then applied, causing the DNA fragments to move through the gel matrix at different rates based on their size and charge. As the fragments move through the gel matrix, they pass by a detector that records the signal and generates a chromatogram.

The chromatogram is a graph that shows the intensity of the signal over time. Each peak in the chromatogram represents a different DNA fragment, and the position of the peak corresponds to the size of the fragment. The color of the peak represents the base that was detected at that position. By analyzing the chromatogram, scientists can determine the sequence of the DNA.

However, not all peaks in the chromatogram are equally reliable. Some peaks may be due to noise or artifacts in the signal, while others may be due to errors in the sequencing process. To determine the quality of each base call, specialized software is used to analyze the chromatogram and assign a quality score to each base call.

The quality score is typically represented as a Phred score, which is a logarithmic value that corresponds to the probability of an error in the base call. For example, a Phred score of 20 corresponds to a 1 in 100 chance of an error, while a Phred score of 40 corresponds to a 1 in 10,000 chance of an error. Generally, a Phred score of 30 or higher is considered to be of high quality and is used for downstream analysis.

To assign a quality score to each base call, the software takes into account several factors, including the intensity of the signal, the background noise, and the position of the peak relative to neighboring peaks. The software also uses a reference sequence to compare the base calls and identify any potential errors.

In addition to quality scoring, the software also performs base calling, which is the process of assigning a specific base (A, C, G, or T) to each peak in the chromatogram. The base calling algorithm takes into account the intensity of the signal and the background noise to make a confident call for each base. However, in regions of the chromatogram where the signal is weak or noisy, the base calling algorithm may assign a low-quality base call or leave the position as ambiguous.

Once the DNA sequence has been determined and the quality scores assigned, the data is ready for downstream analysis. This may include aligning the sequence to a reference genome, identifying genetic variants, or performing functional analysis.

In summary, DNA sequencing involves several steps, from the wet-lab to chromatograms. Capillary electrophoresis is used to separate DNA fragments based on size and charge, generating a chromatogram that can be analyzed using specialized software. The software assigns quality scores to each base call, taking into account factors such as signal intensity and background noise. The resulting DNA sequence can be used for downstream analysis, providing valuable insights into genetics and molecular biology.
