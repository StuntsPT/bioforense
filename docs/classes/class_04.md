### Classe #4 = "Phylogenetics in a nutshell"

#### Biologia Forense 2023-2024

![Logo FCUL](c03_assets/logo-FCUL.png)

Francisco Pina Martins

---

### Summary

* &shy;<!-- .element: class="fragment" -->What is phylogenetics?
* &shy;<!-- .element: class="fragment" -->Alignments
* &shy;<!-- .element: class="fragment" -->Inference methods
* &shy;<!-- .element: class="fragment" -->Practical exercises

---

### Finer than species level

* &shy;<!-- .element: class="fragment" -->Sometimes simply identifying a sample against a database is insufficient
* &shy;<!-- .element: class="fragment" -->In these cases, a finer resolution approach is required
    * &shy;<!-- .element: class="fragment" -->[Illegal species trafficking](https://doi.org/10.1016/j.forsciint.2020.110530)
    * &shy;<!-- .element: class="fragment" -->[Pathogen contact-tracing](https://journals.lww.com/aidsonline/Fulltext/2018/03130/Phylogenetic_analysis_as_a_forensic_tool_in_HIV.2.aspx)

&shy;<!-- .element: class="fragment" -->![HIV phylogenetic tree](c04_assets/HIV-phylogenetic_hypothesis.jpeg)

---

### What is phylogenetics?

* &shy;<!-- .element: class="fragment" -->A set of methods to infer relations between [OTUs](https://link.springer.com/article/10.1007/s12223-018-0627-y)
* &shy;<!-- .element: class="fragment" -->It's used in:
    * &shy;<!-- .element: class="fragment" -->Taxonomy
    * &shy;<!-- .element: class="fragment" -->Molecular dating
    * &shy;<!-- .element: class="fragment" -->Molecular evolution
    * &shy;<!-- .element: class="fragment" -->Gene transfer detection

&shy;<!-- .element: class="fragment" -->![Darwin's Phylo tree](c04_assets/darwin.jpg)

|||

### The origins

<img src="c04_assets/Hennig.jpg" width="18%" class="fragment" style="background:none; border:none; box-shadow:none; float:right">

<div style="width: 60%;">

* &shy;<!-- .element: class="fragment" -->Willi Hennig
    * &shy;<!-- .element: class="fragment" -->*Phylogenetic Systematics*
    * &shy;<!-- .element: class="fragment" -->1955
* &shy;<!-- .element: class="fragment" -->Introduces the concept of "cladograms"
    * &shy;<!-- .element: class="fragment" -->Show relations between taxa
    * &shy;<!-- .element: class="fragment" -->Grouping is based on traits
    * &shy;<!-- .element: class="fragment" -->Similarities are assumed to be of common ancestry

</div>

<img src="c04_assets/cladogram.png" width="19%" class="fragment" style="background:none; border:none; box-shadow:none; position:absolute;bottom: -250px;right: -10px;">

|||

### The origins Pt. 2

<img src="c04_assets/CS-E.jpg" width="20%" class="fragment" style="background:none; border:none; box-shadow:none; float:right">

<div style="width: 70%;">

* &shy;<!-- .element: class="fragment" -->Cavalli-Sforza & Edwards
   * &shy;<!-- .element: class="fragment" -->*Phylograms*
   * &shy;<!-- .element: class="fragment" -->1963

</div>

<img src="c04_assets/CS_phylo.jpg" width="30%" class="fragment" style="background:none; border:none; box-shadow:none; position:absolute;bottom: -100px;right: 320px;">
<img src="c04_assets/CS_migrations.jpg" class="fragment" style="background:none; border:none; box-shadow:none; position:absolute;bottom: -100px;left: -30px;">

---

### Phylogenetic concepts

<img class="fragment" src="c04_assets/Phylo_01.png" style="background:none; border:none; box-shadow:none;">

|||

### Phylogenetic concepts

<img class="fragment" src="c04_assets/Phylo_02.png" style="background:none; border:none; box-shadow:none;">

---

### Rooted Vs. Unrooted Trees

<img class="fragment" src="c04_assets/tree_roots.png" style="background:none; border:none; box-shadow:none;">

---

### Analysis step 1: Alignments

* &shy;<!-- .element: class="fragment" -->Phylogenetic analysis are about performing comparisons
* &shy;<!-- .element: class="fragment" -->Data needs to be **comparable**
    * &shy;<!-- .element: class="fragment" --><a href="https://bio.libretexts.org/Bookshelves/Microbiology/Microbiology_(Boundless)/07%3A_Microbial_Genetics/7.13%3A_Bioinformatics/7.13C%3A_Homologs_Orthologs_and_Paralogs">*Homology*</a> is required

&shy;<!-- .element: class="fragment" -->![Aligned and unaligned sequences](c04_assets/unalign_vs_align.png)

---

### Aligning sequences

* &shy;<!-- .element: class="fragment" -->Let's try to align 2 sequences

<div class="fragment" style="width:30%;margin:auto">

```text
ATGCCCTGA
CATGCCCGG
```

</div>

* &shy;<!-- .element: class="fragment" -->This is done through adding *gaps* (-)

</br>

<div class="fragment">
<div style="width:50%;float:left">

```text
-ATGCCCTGA
CATGCCC-GG
```

</div>
<div style="width:50%;float:right">

```text
-ATGCCCTGA-
CATGCCC-G-G
```

</div>
</div>

* &shy;<!-- .element: class="fragment" -->But which alternative is better?

&shy;<!-- .element: class="fragment" -->![High score](c04_assets/newhighscore.jpg)

|||

### Scoring alignments

* &shy;<!-- .element: class="fragment" -->Gap penalty function
    * &shy;<!-- .element: class="fragment" -->*w*(*k*) indicates the cost of opening a gap of length *k*
* &shy;<!-- .element: class="fragment" -->Substitution matrix
    * &shy;<!-- .element: class="fragment" -->*s*(*a*,*b*) indicates the cost of aligning *a* with *b*
* &shy;<!-- .element: class="fragment" -->Different algorithms will score these in different ways

---

### Analysis step 2: Phylogenetic reconstruction methods

* &shy;<!-- .element: class="fragment" data-fragment-index="1" -->There are several methods for phylogenetic reconstruction
* &shy;<!-- .element: class="fragment" data-fragment-index="2" -->Those that use character states
    * &shy;<!-- .element: class="fragment" data-fragment-index="3" -->Maximum parsimony
    * &shy;<!-- .element: class="fragment" data-fragment-index="4" -->Maximum likelihood
    * &shy;<!-- .element: class="fragment" data-fragment-index="5" -->Bayesian inference
* &shy;<!-- .element: class="fragment" data-fragment-index="6" -->Those that use distance measurements
    * &shy;<!-- .element: class="fragment" data-fragment-index="7" -->UPGMA
    * &shy;<!-- .element: class="fragment" data-fragment-index="8" -->Neighbour joining

<img src="c04_assets/tree_of_life.png" class="fragment" style="background:none; border:none; box-shadow:none; position:absolute;bottom: -180px;right: -200px;">

---

### Distance methods

<div style="float:left; width:65%">

* &shy;<!-- .element: class="fragment" -->Count the number of differences between *taxa*
    * &shy;<!-- .element: class="fragment" -->**Extremely** fast
    * &shy;<!-- .element: class="fragment" -->Inaccurate
        * &shy;<!-- .element: class="fragment" -->Discard a lot of information
* &shy;<!-- .element: class="fragment" -->Can be corrected with sequence evolution models

</div>
<div style="float:right; width:35%" class="fragment">
<img src="c04_assets/dist01.png" style="background:none; border:none; box-shadow:none;">
</div>

<div style="float:left; width:65%">

* &shy;<!-- .element: class="fragment" -->Multiple algorithms can be used to build a tree from the distance matrix
    * &shy;<!-- .element: class="fragment" -->Neighbour-Joining (NJ)
    * &shy;<!-- .element: class="fragment" -->**U**nweighted **P**air **G**roup **M**ethod with **A**rithmetic mean (UPGMA)
* &shy;<!-- .element: class="fragment" -->Algorithm choice influences the obtained tree!

</div>

---

### Character state based methods

<div style="float:left; width:65%">

* &shy;<!-- .element: class="fragment" -->Most try *every possible tree* (not Bayesian methods!)
    * &shy;<!-- .element: class="fragment" -->AKA: "Brute force methods"

</div>
<div style="float:right; width:35%" class="fragment">

![All possible trees](c04_assets/possible_trees.png)

</div>
<div style="float:left; width:65%">

* &shy;<!-- .element: class="fragment" -->Use an optimality criterion to select the "best" tree
    * &shy;<!-- .element: class="fragment" -->Most parsimonious
    * &shy;<!-- .element: class="fragment" -->Highest likelihood
        * &shy;<!-- .element: class="fragment" -->Based on *evolutionary models*

</div>

&shy;<!-- .element: class="fragment" -->![Probability modelling](c04_assets/Sub_prob_matrix.png)

---

### Analysis step 3: Support values

* &shy;<!-- .element: class="fragment" -->Numbers on branches indicate *support*

&shy;<!-- .element: class="fragment" -->![Bootstrapping scheme](c04_assets/bootstrap.png)

---

### Guided example (Pt. 1)

* Download [this FASTA file](c04_assets/AK_s7rp.fasta) and open it using Aliview
    * Adapted from: [Akihito et al. (2016)](https://doi.org/10.1016/j.gene.2015.10.014)
* Align the sequences using the menu "Align" -> "Re-align everything"
* Save the aligned sequences using "File" -> "Save as FASTA"
    * The filename is up to you, but ideally have the information in the name that the file is aligned

![Aliview in use](c04_assets/aliview_in_use.png)

|||

### Guided example (Pt. 2)

* Next, open the aligned file with [MEGA](https://megasoftware.net/)
    * It should already be installed in FCUL's machines
* Change the "Missing data" character from "?" to "N" and click "OK"
* The data is **not** protein coding DNA
* Click the "Phylogeny" button and select "Neighbour Joining"
    * Change "Test of phylogeny" to "Bootstrap"
    * 100 iterations will suffice
    * Click OK, and you should see a tree
        * Play around with the settings, until you are able to interpret the three main monophyletic groups in the figure
        * Click "Image" -> "Save as PNG" and save your tree for posterity

|||

### Guided example (Pt. 2)

* Close the tree window
* Click the "Phylogeny" button and select "Maximum Likelihood"
    * Change "Test of phylogeny" to "Bootstrap"
    * 100 iterations will suffice
    * Change the model to "General Time Reversible"
    * Under "Rates and Patterns" choose "(G+I)"
        * These are the same setting as used in the original paper
    * Use more than 1 thread, this analysis is longer
    * Click OK, and you should see a tree (this might take a while)
        * Play around with the settings, until you are able to interpret the three main monophyletic groups in the figure
        * Click "Image" -> "Save as PNG" and save your tree for posterity

---

### Final Challenge

* Consider the paper [Siljic et al. 2017](https://doi.org/10.1016/j.fsigen.2016.12.006)
* [Here](c04_assets/hiv_env_subjects.fasta) you will find a FASTA file containing six HIV env protein sequences from the three studied subjects
* [Here](c04_assets/accession_list.txt) you will find a list with accession numbers of (some of) the controls used in the paper
* Combine all the sequences in a single file, and perform a phylogenetic analysis
    * Indicate whether the evidence supports the hypothesis that the studied individuals infected each other
* *Hint*: The control sequences are **much** longer than the subjects' sequences. You can "trim" your alignment to optimize processing

---

### References

* [Wildlife forensics: A boon for species identification and conservation implications](https://doi.org/10.1016/j.forsciint.2020.110530)
* [Phylogenetic analysis as a forensic tool in HIV transmission investigations](https://journals.lww.com/aidsonline/Fulltext/2018/03130/Phylogenetic_analysis_as_a_forensic_tool_in_HIV.2.aspx)
* [Molecular phylogenetics for newcommers](https://link.springer.com/chapter/10.1007%2F10_2016_49)
* [Short intro to phylogenetics on NCBI](https://www.ncbi.nlm.nih.gov/books/NBK21122/)
* [Mutation types](https://evolution.berkeley.edu/dna-and-mutations/types-of-mutations/)
* [Sequence alignment "behind the scenes"](https://www.bioinformaticshome.com/bioinformatics_tutorials/sequence_alignment/DNA_scoring_matrices.html)
* [Details on tree searching methods](https://doi.org/10.1002/0471250953.bi0604s00)
* [MEGA manual entry on bootstrapping](https://www.megasoftware.net/mega4/WebHelp/part_iv___evolutionary_analysis/constructing_phylogenetic_trees/statistical_tests_of_a_tree_obtained/bootstrap_tests/hc_bootstrap_test_phylogeny.htm)
* [Akihito et al. 2016](https://doi.org/10.1016/j.gene.2015.10.014)
* [Siljic et al. 2017](https://doi.org/10.1016/j.fsigen.2016.12.006)
