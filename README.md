# Biologia Forense 2023-2024

Aulas da UC "Biologia Forense" do mestrado em Biologia Humana e Ambiente da FCUL (2023-2024).

Slides acessíveis via [gitlab pages](https://stuntspt.gitlab.io/BioForense) .

Estes slides foram feitos em "[github flavoured markdown](https://guides.github.com/features/mastering-markdown/)" com recurso ao sistema [reveal.js](http://lab.hakim.se/reveal-js/). As páginas de exercícios são geradas com recurso ao [Python-markdown](https://python-markdown.github.io/).
